// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCt9ZlUINYC2cbPJlVYN_aa9Rfd_I1tLpo',
    authDomain: 'protech-c81de.firebaseapp.com',
    databaseURL: 'https://protech-c81de.firebaseio.com',
    projectId: 'protech-c81de',
    storageBucket: 'protech-c81de.appspot.com',
    messagingSenderId: '1033263768549',
    appId: '1:1033263768549:web:ec7fbdd966887dd93cc50c',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
