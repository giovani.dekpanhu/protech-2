import { FirebaseService } from './services/firebase/firebase.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ListeProjetsComponent } from './liste-projets/liste-projets.component';
import { HomeConsultantComponent } from './home-consultant/home-consultant.component';
import { VueProjetComponent } from './vue-projet/vue-projet.component';
import { LayoutConsultantComponent } from './layout-consultant/layout-consultant.component';
import { LayoutAuthComponent } from './layout-auth/layout-auth.component';
import { LayoutEnterpriseComponent } from './layout-enterprise/layout-enterprise.component';
import { DescriptionProjetComponent } from './description-projet/description-projet.component';
import { MesProjetsComponent } from './mes-projets/mes-projets.component';
import { VueProjetEnterpriseComponent } from './vue-projet-enterprise/vue-projet-enterprise.component';
import { OffresComponent } from './offres/offres.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthComponent } from './services/auth/auth.component';
import { AuthGuardComponent } from './services/auth-guard/auth-guard.component';
import { SignUpEnterpriseComponent } from './sign-up-enterprise/sign-up-enterprise.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';

import * as firebase from 'firebase';

firebase.initializeApp(environment.firebase);
import { LayoutAnonymeComponent } from './layout-anonyme/layout-anonyme.component';
import { ProfileComponent } from './profile/profile.component';
import { AutresProjetsComponent } from './autres-projets/autres-projets.component';
import { DescriptionProjetConsultantComponent } from './description-projet-consultant/description-projet-consultant.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ListeProjetsComponent,
    HomeConsultantComponent,
    VueProjetComponent,
    LayoutConsultantComponent,
    LayoutAuthComponent,
    LayoutEnterpriseComponent,
    DescriptionProjetComponent,
    MesProjetsComponent,
    VueProjetEnterpriseComponent,
    OffresComponent,
    SignUpComponent,
    SignUpEnterpriseComponent,
    LayoutAnonymeComponent,
    ProfileComponent,
    AutresProjetsComponent,
    DescriptionProjetConsultantComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
  ],
  providers: [AuthComponent, AuthGuardComponent],
  bootstrap: [AppComponent, [FirebaseService]],
})
export class AppModule {}
