import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  IUser,
  ICategorie,
  IProjet,
  FirebaseService,
} from '../services/firebase/firebase.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-liste-projets',
  templateUrl: './liste-projets.component.html',
  styleUrls: ['./liste-projets.component.css'],
})
export class ListeProjetsComponent implements OnInit {
  ajouterProjetForm: FormGroup;
  errorMessage: string;
  users: IUser[] = [];
  categories: ICategorie[] = [];
  projets: IProjet[] = [];
  projetsEnAttente: IProjet[] = [];
  projetsEnCours: IProjet[] = [];
  projetsTermines: IProjet[] = [];
  currentUsers: IUser[] = [];
  cat;
  ids;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    public firebaseService: FirebaseService
  ) {}

  ngOnInit(): void {
    this.getCurrentProjetCategories();
  }

  // tslint:disable-next-line: typedef
  getCurrentProjetCategories() {
    this.cat = this.route.snapshot.paramMap.get('libelle');
    this.ids = this.route.snapshot.paramMap.get('id');

    firebase
      .firestore()
      .collection('projet')
      .where('categorieId', '==', this.ids)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.projets.push({
              id: doc.id,
              ...(doc.data() as IProjet),
            });
          });
        },
        () => {
          alert('Aucun prijet dans cette categorie !!');
        }
      );
  }
}
