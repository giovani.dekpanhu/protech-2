import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import {
  FirebaseService,
  ICategorie,
  IProjet,
  IUser,
} from '../services/firebase/firebase.service';

@Component({
  selector: 'app-vue-projet-enterprise',
  templateUrl: './vue-projet-enterprise.component.html',
  styleUrls: ['./vue-projet-enterprise.component.css'],
})
export class VueProjetEnterpriseComponent implements OnInit {
  users: IUser[] = [];
  categories: ICategorie[] = [];
  projet: IProjet[];
  currentUsers: IUser[] = [];
  ids;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    public firebaseService: FirebaseService,
  ) {}

  ngOnInit(): void {
    this.getCurrentProjet();
  }

  getCurrentProjet(): void {
    this.ids = this.route.snapshot.paramMap.get('id');
    this.firebaseService.getProjets().subscribe((res) => {
      this.projet = res.map((p) => {
        return {
          id: p.payload.doc.id,
          ...(p.payload.doc.data() as IProjet),
        } as IProjet;
      });
      console.log(this.projet);
    });
  }
}
