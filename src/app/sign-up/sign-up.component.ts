import { IUser, FirebaseService } from './../services/firebase/firebase.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthComponent } from './../services/auth/auth.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
  signupForm: FormGroup;
  errorMessage: string;
  userList: IUser[] = [];
  userDetails: IUser;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthComponent,
    private router: Router,
    private firebaseService: FirebaseService,
  ) { }

  ngOnInit(): void {
    this.initForm();
    // this.getUsers();
    console.log(this.userList);
  }

  // openModal(userId: string): void {
  //   this.userDetails = this.userList.find((user: IUser) => user.id === userId);

  //   this.initForm(this.userDetails);
  // }

  initForm(): void {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)],
      ],
      nomComplet: ['', [Validators.required]],
      telephone: ['', [Validators.required]],
      competences: ['', [Validators.required]],
    });
  }

  onSubmit() {
    const email = this.signupForm.get('email').value;
    const password = this.signupForm.get('password').value;
    const nomComplet = this.signupForm.get('nomComplet').value;
    const telephone = this.signupForm.get('telephone').value;
    const competences = this.signupForm.get('competences').value;

    const data = {
      email,
      nomComplet,
      telephone,
      competences,
      role: 'consultant',
    };

    // console.log(email, password, nomComplet, telephone, competences);

    this.authService.createNewUser(email, password).then(
      () => {
        this.addUser(data);
        // this.getUsers();
        // console.log(this.userList);
        this.router.navigate(['/consultant/home']);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }

  // getUsers(): void {
  //   this.firebaseService.getUsers().subscribe((res) => {
  //     this.userList = res.map((user) => {
  //       return {
  //         ...user.payload.doc.data(),
  //         id: user.payload.doc.id
  //       } as IUser;
  //     });
  //   });
  // }

  addUser(data: IUser): void {
    // console.log('Entrée dans addUser 1');
    this.firebaseService.addUser(data).then();
    // console.log('Sortie de addUser 1');
  }

  // updateUser(userId: string): void {
  //   this.firebaseService.updateUser(userId, this.signupForm.value).then();
  // }

  // deleteUser(userId: string): void {
  //   this.firebaseService.deleteUser(userId).then();
  // }
}
