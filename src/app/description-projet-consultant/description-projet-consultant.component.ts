import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import {
  FirebaseService,
  ICategorie,
  IProjet,
  IUser,
} from '../services/firebase/firebase.service';

@Component({
  selector: 'app-description-projet-consultant',
  templateUrl: './description-projet-consultant.component.html',
  styleUrls: ['./description-projet-consultant.component.css']
})
export class DescriptionProjetConsultantComponent implements OnInit {
  projet: IProjet[];
  ids;
  currentUsers: IUser[] = [];
  postuleDeja: boolean;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    public firebaseService: FirebaseService
  ) { }

  ngOnInit(): void {
    this.postuleDeja = false;
    this.getCurrentProjet();
  }

  getCurrentProjet(): void {
    this.ids = this.route.snapshot.paramMap.get('id');
    this.firebaseService.getProjets().subscribe((res) => {
      this.projet = res.map((p) => {
        return {
          id: p.payload.doc.id,
          ...(p.payload.doc.data() as IProjet),
        } as IProjet;
      });
      console.log(this.projet);
    });
  }

  postuler(id) {
    firebase
      .firestore()
      .collection('user')
      .where('email', '==', firebase.auth().currentUser.email)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.currentUsers.push({
              id: doc.id,
              ...(doc.data() as IUser),
            });
          });
          this.firebaseService.addCandidature({
            candidatId: this.currentUsers[0].id,
            projetId: id,
          });
          this.postuleDeja = true;
          alert('Votre candidature a bien été prise en compte');
        },
        () => { }
      );
  }

}
