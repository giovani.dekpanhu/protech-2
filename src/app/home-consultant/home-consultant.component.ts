import { IUser, ICategorie, FirebaseService, IProjet, IMembreProjet } from './../services/firebase/firebase.service';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-consultant',
  templateUrl: './home-consultant.component.html',
  styleUrls: ['./home-consultant.component.css'],
})
export class HomeConsultantComponent implements OnInit {
  projetsDiriges: IProjet[] = [];
  projetsInterventifs: IProjet[] = [];
  currentUsers: IUser[] = [];
  membres: IMembreProjet[] = [];

  constructor(
    private router: Router,
    public firebaseService: FirebaseService
  ) {}

  ngOnInit(): void {
    this.getCurrentUsers();
    // this.getProjets();
  }

  getProjets(id) {
    console.log(id);
    firebase
      .firestore()
      .collection('projet')
      .where('chefProjetId', '==', id)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            console.log(doc.id);
            this.projetsDiriges.push({
              id: doc.id,
              ...(doc.data() as IProjet),
            });
          });
        },
        () => {
          // alert("Vous ne dirigez aucun projet");
        }
      );
  }

  getCurrentUsers() {
    firebase
      .firestore()
      .collection('user')
      .where('email', '==', firebase.auth().currentUser.email)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.currentUsers.push({
              id: doc.id,
              ...(doc.data() as IUser),
            });
          });
          this.getProjets(this.currentUsers[0].id);
          firebase
            .firestore()
            .collection('membre')
            .where('membreId', '==', this.currentUsers[0].id)
            .get()
            .then(
              (re) => {
                re.forEach((doc) => {
                  this.membres.push({
                    id: doc.id,
                    ...(doc.data() as IMembreProjet),
                  } as IMembreProjet);
                });
                console.log('Membres projets 2 : ', this.membres);

                this.membres.forEach((m) => {
                  firebase
                    .firestore()
                    .collection('projet')
                    .doc(m.projetId)
                    .get()
                    .then((r) => {
                      this.projetsInterventifs.push({
                        id: r.id,
                        ...(r.data() as IProjet),
                      });
                    });
                });
                console.log('Projets interventifs ', this.projetsInterventifs);
              },
              () => {}
            );
        },
        () => {}
      );
  }
}
