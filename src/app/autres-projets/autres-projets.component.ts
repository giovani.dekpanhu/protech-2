import { Component, OnInit } from '@angular/core';
import { IProjet, IUser, ICandidature, FirebaseService } from '../services/firebase/firebase.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-autres-projets',
  templateUrl: './autres-projets.component.html',
  styleUrls: ['./autres-projets.component.css']
})
export class AutresProjetsComponent implements OnInit {
  projets: IProjet[] = [];
  currentUsers: IUser[] = [];

  constructor(public firebaseService: FirebaseService) { }

  ngOnInit(): void {
    this.getProjets();
  }

  getProjets() {
    firebase
      .firestore()
      .collection('projet')
      .where('visible', '==', 'public')
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            console.log(doc.id);
            this.projets.push({
              id: doc.id,
              ...(doc.data() as IProjet),
            });
          });
        },
        () => {
          // alert("Vous ne dirigez aucun projet");
        }
      );
  }

  postuler(id) {
    firebase
      .firestore()
      .collection('user')
      .where('email', '==', firebase.auth().currentUser.email)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.currentUsers.push({
              id: doc.id,
              ...(doc.data() as IUser),
            });
          });
          this.firebaseService.addCandidature({
            candidatId: this.currentUsers[0].id,
            projetId: id,
          });
          alert('Votre candidature a bien été prise en compte');
        },
        () => { }
      );
  }

}
