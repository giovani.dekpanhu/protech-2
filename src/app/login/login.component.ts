import { FirebaseService, IUser } from './../services/firebase/firebase.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthComponent } from './../services/auth/auth.component';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: string;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthComponent,
    private router: Router,
    private afs: AngularFirestore,
    private firebaseService: FirebaseService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)],
      ],
    });
  }

  onSubmit() {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;

    this.authService.signInUser(email, password).then(
      () => {
        const users = ['salut'];
        const user = firebase
          .firestore()
          .collection('user')
          .where('email', '==', firebase.auth().currentUser.email)
          .get()
          .then(
            (res) => {
              res.forEach((doc) => {
                // console.log(doc.id, doc.data().email);
                users.push(doc.data().role);
                this.firebaseService.currentUser = {
                  ...doc.data() as IUser
                };
              });
              console.log(users[1]);
              if (users[1] === 'porteur') {
                this.router.navigate(['/entreprise/home']);
              } else {
                this.router.navigate(['/consultant/home']);
              }
            },
            () => {}
          );
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }
}
