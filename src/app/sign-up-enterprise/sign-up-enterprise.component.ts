import { IUser, FirebaseService } from './../services/firebase/firebase.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthComponent } from './../services/auth/auth.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up-enterprise',
  templateUrl: './sign-up-enterprise.component.html',
  styleUrls: ['./sign-up-enterprise.component.css']
})
export class SignUpEnterpriseComponent implements OnInit {
  signupForm: FormGroup;
  errorMessage: string;
  userList: IUser[] = [];
  userDetails: IUser;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthComponent,
    private router: Router,
    private firebaseService: FirebaseService,
  ) { }

  ngOnInit(): void {
    this.initForm();
    console.log(this.userList);
  }

  initForm(): void {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)],
      ],
      nomComplet: ['', [Validators.required]],
      telephone: ['', [Validators.required]],
      chefEntreprise: ['', [Validators.required]],
      adresseEntreprise: ['', [Validators.required]],
    });
  }

  onSubmit() {
    const email = this.signupForm.get('email').value;
    const password = this.signupForm.get('password').value;
    const nomComplet = this.signupForm.get('nomComplet').value;
    const telephone = this.signupForm.get('telephone').value;
    const adresseEntreprise = this.signupForm.get('adresseEntreprise').value;
    const chefEntreprise = this.signupForm.get('chefEntreprise').value;

    const data = {
      email,
      nomComplet,
      telephone,
      adresseEntreprise,
      chefEntreprise,
      role: 'porteur',
    };

    this.authService.createNewUser(email, password).then(
      () => {
        this.addUser(data);
        this.router.navigate(['/entreprise/home']);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }

  addUser(data: IUser): void {
    this.firebaseService.addUser(data).then();
  }

}
