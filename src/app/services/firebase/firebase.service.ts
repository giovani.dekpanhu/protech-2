import { Injectable, Component } from '@angular/core';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  currentUser: IUser;

  constructor(private afs: AngularFirestore) {}

  // tslint:disable: typedef

  getUsers() {
    return this.afs.collection('user').snapshotChanges();
  }

  addUser(payload: IUser) {
    return this.afs.collection('user').add(payload);
  }

  updateUser(userId: string, payload: IUser) {
    return this.afs.doc('user/' + userId).update(payload);
  }

  deleteUser(userId: string) {
    return this.afs.doc('user/' + userId).delete();
  }

  // For categories

  getCategories() {
    return this.afs.collection('categorie').snapshotChanges();
  }

  getProjets() {
    return this.afs.collection('projet').snapshotChanges();
  }

  // For projets

  addProjet(payload: IProjet) {
    return this.afs.collection('projet').add(payload);
  }

  getCurrentProjet(id: number) {
    return new Promise((resolve, reject) => {
      firebase
        .database()
        .ref('/projet/' + id)
        .once('value')
        .then(
          (data: DataSnapshot) => {
            resolve(data.val());
          },
          (error) => {
            reject(error);
          }
        );
    });
  }

  // For tasks

  addTache(payload: ITache) {
    return this.afs.collection('tache').add(payload);
  }

  updateTache(tacheId: string, payload: ITache) {
    return this.afs.doc('tache/' + tacheId).update(payload);
  }

  updateProjet(projetId: string, payload: IProjet) {
    return this.afs.doc('projet/' + projetId).update(payload);
  }

  deleteTache(tacheId: string) {
    return this.afs.doc('tache/' + tacheId).delete();
  }

  // For Candidatures

  addCandidature(payload: ICandidature) {
    return this.afs.collection('candidature').add(payload);
  }

  // For members

  addMembreProjet(payload: IMembreProjet) {
    console.log('Arrivé');
    return this.afs.collection('membre').add(payload);
  }
}

export interface IUser {
  id?: string;
  email: string;
  nomComplet?: string;
  telephone?: string;
  competences?: string;
  chefEntreprise?: string;
  adresseEntreprise?: string;
  avatarUrl?: string;
  role?: string;
  deleted?: boolean;
}

export interface IProjet {
  id?: string;
  nom: string;
  descriptionSuccinte?: string;
  description?: string;
  dateDebutPrevue?: string;
  dateFinPrevue?: string;
  statut?: string;
  validated?: boolean;
  dateFinEffective?: string;
  visible?: boolean;
  imageUrl?: string;
  categorieId?: string;
  chefProjetId?: string;
  porteurId?: string;
  deleted?: boolean;
}

export interface ITache {
  id?: string;
  nom?: string;
  description?: string;
  dateLimite?: string;
  statut?: string;
  validated?: boolean;
  dateFinEffective?: string;
  chargeTacheId?: string;
  projetId?: string;
  deleted?: boolean;
  phase?: string;
  chargeTacheNom?: string;
  chargeTacheUrl?: string;
}

export interface ICategorie {
  id?: string;
  libelle?: string;
  imageUrl?: string;
}

export interface ICandidature {
  id?: string;
  candidatId?: string;
  projetId?: string;
}

export interface IMembreProjet {
  id?: string;
  membreId?: string;
  projetId?: string;
}
