import { IUser, ICategorie, FirebaseService, IProjet } from './../services/firebase/firebase.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mes-projets',
  templateUrl: './mes-projets.component.html',
  styleUrls: ['./mes-projets.component.css'],
})
export class MesProjetsComponent implements OnInit {
  ajouterProjetForm: FormGroup;
  errorMessage: string;
  users: IUser[] = [];
  categories: ICategorie[] = [];
  projets: IProjet[] = [];
  projetsEnAttente: IProjet[] = [];
  projetsEnCours: IProjet[] = [];
  projetsTermines: IProjet[] = [];
  currentUsers: IUser[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public firebaseService: FirebaseService
  ) {}

  ngOnInit(): void {
    this.getCurrentUsers();
    this.getProjets();
    this.initForm();
    this.getCategories();
    this.getUsers();
  }

  initForm(): void {
    this.ajouterProjetForm = this.formBuilder.group({
      nom: ['', [Validators.required, Validators.email]],
      description: ['', [Validators.required]],
      descriptionSuccinte: ['', [Validators.required]],
      dateDebutPrevue: ['', [Validators.required]],
      dateFinPrevue: ['', [Validators.required]],
      categorieId: ['', [Validators.required]],
      chefProjetId: ['', [Validators.required]],
      visible: ['public', [Validators.required]],
    });
  }

  onSubmit(): void {
    const data = {
      nom: this.ajouterProjetForm.get('nom').value,
      description: this.ajouterProjetForm.get('description').value,
      descriptionSuccinte: this.ajouterProjetForm.get('descriptionSuccinte')
        .value,
      dateDebutPrevue: this.ajouterProjetForm.get('dateDebutPrevue').value,
      dateFinPrevue: this.ajouterProjetForm.get('dateFinPrevue').value,
      categorieId: this.ajouterProjetForm.get('categorieId').value,
      chefProjetId: this.ajouterProjetForm.get('chefProjetId').value,
      visible: this.ajouterProjetForm.get('visible').value,
      statut: 'En attente',
      validated: false,
      porteurId: firebase.auth().currentUser.uid,
    };

    console.log(firebase.auth().currentUser.uid);

    this.addProjet(data);

    alert('Le projet a bien été ajouté');

    this.initForm();
  }

  getUsers(): void {
    this.firebaseService.getUsers().subscribe((res) => {
      this.users = res.map((user) => {
        return {
          id: user.payload.doc.id,
          ...(user.payload.doc.data() as IUser),
        } as IUser;
      });
      console.log(this.users);
    });
  }

  getCategories(): void {
    this.firebaseService.getCategories().subscribe((res) => {
      this.categories = res.map((c) => {
        return {
          id: c.payload.doc.id,
          ...(c.payload.doc.data() as ICategorie),
        } as ICategorie;
      });
      console.log(this.categories);
    });
  }

  addProjet(data: IProjet): void {
    this.firebaseService.addProjet(data).then((res) => {
      res.get().then((r) => {
        this.projets.push({
          id: r.id,
          ...(r.data() as IProjet),
        });
        this.projetsEnAttente.push({
          id: r.id,
          ...(r.data() as IProjet),
        });
      });
    });
  }

  // tslint:disable: typedef
  getProjets() {
    firebase
      .firestore()
      .collection('projet')
      .where('porteurId', '==', firebase.auth().currentUser.uid)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.projets.push({
              id: doc.id,
              ...(doc.data() as IProjet),
            });
          });
          this.projetsEnAttente = this.projets.filter(this.filtrerEnAttente);
          this.projetsEnCours = this.projets.filter(this.filtrerEnCours);
          this.projetsTermines = this.projets.filter(this.filtrerTermines);
        },
        () => {
          alert('Vous n\'avez enregistré aucun projet');
        }
      );
  }

  getCurrentUsers() {
    firebase
      .firestore()
      .collection('user')
      .where('email', '==', firebase.auth().currentUser.email)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.currentUsers.push({
              id: doc.id,
              ...(doc.data() as IUser),
            });
          });
        },
        () => {
        }
    );
  }

  filtrerEnAttente(abc) {
    return abc.statut === 'En attente';
  }

  filtrerEnCours(abc) {
    return abc.statut === 'En cours';
  }

  filtrerTermines(abc) {
    return abc.statut === 'Terminé';
  }
}
