import { AuthComponent } from './../services/auth/auth.component';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-layout-enterprise',
  templateUrl: './layout-enterprise.component.html',
  styleUrls: ['./layout-enterprise.component.css']
})
export class LayoutEnterpriseComponent implements OnInit {
  isAuth: boolean;

  constructor(private authComponent: AuthComponent) { }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.isAuth = true;
      } else {
        this.isAuth = false;
      }
    });
  }

  onSignOut(): void {
    this.authComponent.signOutUser();
  }
}
