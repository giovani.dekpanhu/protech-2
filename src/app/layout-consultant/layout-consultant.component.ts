import { AuthComponent } from './../services/auth/auth.component';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-layout-consultant',
  templateUrl: './layout-consultant.component.html',
  // tslint:disable-next-line: max-line-length
  styleUrls: ['./layout-consultant.component.css'],
})
export class LayoutConsultantComponent implements OnInit {
  isAuth: boolean;

  constructor(private authComponent: AuthComponent) {}

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.isAuth = true;
      } else {
        this.isAuth = false;
      }
    });
  }

  onSignOut(): void {
    this.authComponent.signOutUser();
  }
}
